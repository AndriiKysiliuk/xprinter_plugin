var exec = require('cordova/exec');

module.exports.connect = function (arg0,success, error){
    exec(success , error, 'XPrinter' , 'connect' , [arg0]);
}

module.exports.disconnect = function (arg0,success, error){
    exec(success , error, 'XPrinter' , 'disconnect' , [arg0]);
}

module.exports.printCheck = function (arg0,success, error){
    exec(success , error, 'XPrinter' , 'printCheck' , [arg0]);
}

module.exports.printWaste = function (arg0,success, error){
    exec(success , error, 'XPrinter' , 'printWaste' , [arg0]);
}

module.exports.printBarcode = function (arg0,success, error){
    exec(success , error, 'XPrinter' , 'printBarcode' , [arg0]);
}

module.exports.printQRcode = function (arg0,success, error){
    exec(success , error, 'XPrinter' , 'printQRcode' , [arg0]);
}
