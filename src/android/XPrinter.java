package cordova.plugin.xprinter;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.content.ComponentName;
import android.util.Log;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import net.posprinter.posprinterface.IMyBinder;
import net.posprinter.posprinterface.ProcessData;
import net.posprinter.posprinterface.TaskCallback;
import net.posprinter.service.PosprinterService;
import net.posprinter.utils.DataForSendToPrinterPos80;

import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.content.Context.BIND_AUTO_CREATE;

/**
 * This class echoes a string called from JavaScript.
 */
public class XPrinter extends CordovaPlugin {

    private String printerIP;
    private Boolean repeatCheckPrint;
    private Boolean repeatWastePrint;

    private static IMyBinder binder;
    public Context context;
    private static boolean ISCONNECT;
    public CordovaInterface cordova;


    ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            //Bind successfully
            binder = (IMyBinder) iBinder;
            Log.e("binder", "connected");
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.e("disbinder", "disconnected");
        }
    };

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);

        this.cordova = cordova;
        context = cordova.getActivity().getApplicationContext();
        Intent intent = new Intent(cordova.getActivity(), PosprinterService.class);
        context.bindService(intent, conn, BIND_AUTO_CREATE);

        repeatCheckPrint = true;
        repeatWastePrint = true;

        Log.e("XPrinter", "Initialize cordova-plugin-xprinter");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binder.DisconnectCurrentPort(new TaskCallback() {
            @Override
            public void OnSucceed() {

            }

            @Override
            public void OnFailed() {

            }
        });
        context.unbindService(conn);
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        switch (action) {
            case "connect":
                this.connect(args, callbackContext);
                return true;
            case "disconnect":
                this.disconnect(callbackContext);
                return true;
            case "printCheck":
                this.reConnect("check", args, callbackContext);
                return true;
            case "printWaste":
                this.reConnect("waste", args, callbackContext);
                return true;
            case "printBarcode":
                this.printBarcode(args, callbackContext);
                return true;
            case "printQRcode":
                this.printQRcode(args, callbackContext);
                return true;
            default:
                return false;
        }
    }

    private void connect(JSONArray args, CallbackContext callback) {
        if (args != null) {
            try {
                String ipAddress = args.getJSONObject(0).getString("ip");
                this.printerIP = ipAddress;
                if (ipAddress.equals(null) || ipAddress.equals("")) {
                    callback.error("IP address is empty");
                } else {
                    //ipAddress :ip address; portal:9100

                    binder.ConnectNetPort(ipAddress, 9100, new TaskCallback() {
                        @Override
                        public void OnSucceed() {
                            ISCONNECT = true;
                            callback.success("Connect success");
                        }

                        @Override
                        public void OnFailed() {
                            ISCONNECT = false;
                            callback.error("Connect failed");
                        }
                    });
                }
            } catch (Exception ex) {
                callback.error("Something went wrong " + ex);
            }
        } else {
            callback.error("Please donot pass null value");
        }
    }

    private void disconnect(CallbackContext callback) {
        if (ISCONNECT) {
            binder.DisconnectCurrentPort(new TaskCallback() {
                @Override
                public void OnSucceed() {
                    ISCONNECT = false;
                    callback.success("Disconnect successful");
                }

                @Override
                public void OnFailed() {
                    ISCONNECT = true;
                    callback.error("Disconnect failed");
                }
            });
        }
    }

    private void printCheck(JSONArray args, CallbackContext callback) {
        if (ISCONNECT) {
            if (args != null) {
                PrintCheck printCheck = this.parseJsonCheck(args);
                try {
                    binder.WriteSendData(
                            new TaskCallback() {
                                @Override
                                public void OnSucceed() {

                                }

                                @Override
                                public void OnFailed() {
                                    callback.error("Disconnect failed");
                                }
                            }, new ProcessData() {
                                @Override
                                public List<byte[]> processDataBeforeSend() {

                                    List<byte[]> list = new ArrayList<byte[]>();
                                    //initialize the printer
                                    list.add(DataForSendToPrinterPos80.initializePrinter());

                                    list.add(DataForSendToPrinterPos80.selectAlignment(1));
                                    list.add(DataForSendToPrinterPos80.selectCharacterSize(16));
                                    list.add(DataForSendToPrinterPos80.selectOrCancelBoldModel(1));
                                    byte[] shopName = StringUtils.strTobytes(printCheck.shopName + '\n');
                                    list.add(shopName);

                                    list.add(DataForSendToPrinterPos80.selectCharacterSize(0));
                                    list.add(DataForSendToPrinterPos80.selectOrCancelBoldModel(2));
                                    if (!printCheck.fop.isEmpty()) {
                                        byte[] fop = StringUtils.strTobytes("ФОП: " + printCheck.fop + '\n');
                                        list.add(fop);
                                    }

                                    if (!printCheck.ipn.isEmpty()) {
                                        byte[] ipn = StringUtils.strTobytes("РНОКПП: " + printCheck.ipn + '\n' + '\n');
                                        list.add(ipn);
                                    } else {
                                        list.add(StringUtils.strTobytes("\n\n"));
                                    }

                                    list.add(DataForSendToPrinterPos80.selectAlignment(0));
                                    byte[] employee = StringUtils.strTobytes("Працівник: " + printCheck.employee + '\n');
                                    list.add(employee);

                                    byte[] saleDate = StringUtils.strTobytes("Дата продажу: " + printCheck.saleDate + '\n' + '\n');
                                    list.add(saleDate);

                                    byte[] header = StringUtils.strTobytes("          Назва            К-сть   Ціна   Сума  ");
                                    list.add(header);

                                    byte[] devider = new byte[48];
                                    Arrays.fill(devider, (byte) 173);
                                    list.add(devider);

                                    int nameLenght = 26;
                                    int priceLenght = 6;
                                    int quantityLenght = 6;
                                    int sumLenght = 7;

                                    for (int i = 0; i < printCheck.products.size(); i++) {
                                        PrintProduct pr = printCheck.products.get(i);
                                        if (pr.name.length() > nameLenght) {

                                            String[] arr = pr.name.split(" ");
                                            String name = "";

                                            String tempWord = "";
                                            for (int j = 0; j < arr.length; j++) {
                                                if (tempWord.length() + arr[j].length() > nameLenght) {
                                                    tempWord += "\n";
                                                    name += tempWord;
                                                    tempWord = arr[j] + " ";

                                                    if (j == arr.length - 1) {
                                                        name += arr[j];
                                                    }
                                                } else {
                                                    if (j == arr.length - 1) {
                                                        tempWord += arr[j];
                                                        name += tempWord;
                                                    } else {
                                                        tempWord += arr[j] + " ";
                                                    }
                                                }
                                            }

                                            String[] nameArr = name.split("\n");
                                            String lastWord = nameArr[nameArr.length - 1];
                                            String formatedWord = formatValue(lastWord, nameLenght, true);
                                            String fullName = name.replace(lastWord, formatedWord);

                                            byte[] nameByte = StringUtils.strTobytes(fullName);
                                            list.add(nameByte);
                                        } else {
                                            byte[] name = StringUtils.strTobytes(formatValue(pr.name, nameLenght, true));
                                            list.add(name);
                                        }

                                        byte[] quantity = StringUtils.strTobytes(" " + formatValue(pr.quantity, quantityLenght, false));
                                        list.add(quantity);

                                        byte[] price = StringUtils.strTobytes(" " + formatValue(pr.price, priceLenght, false));
                                        list.add(price);

                                        byte[] sum = StringUtils.strTobytes(formatValue(pr.sum, sumLenght, false) + " \n");
                                        list.add(sum);
                                    }

                                    list.add(devider);

                                    if (!printCheck.client.isEmpty()) {
                                        byte[] client = StringUtils.strTobytes("Клієнт: " + printCheck.client + "\n\n");
                                        list.add(client);
                                    }

                                    list.add(DataForSendToPrinterPos80.selectOrCancelBoldModel(1));
                                    if (!printCheck.discount.isEmpty()) {
                                        byte[] discount = StringUtils.strTobytes("Знижка:");
                                        list.add(discount);
                                        byte[] discountValue = StringUtils.strTobytes(formatValue(printCheck.discount + " грн.", 41, false));
                                        list.add(discountValue);
                                    }

                                    if (!printCheck.bonusPay.isEmpty()) {
                                        byte[] discount = StringUtils.strTobytes("Оплата бонусами:");
                                        list.add(discount);
                                        byte[] discountValue = StringUtils.strTobytes(formatValue(printCheck.bonusPay + " грн.", 32, false));
                                        list.add(discountValue);
                                    }

                                    byte[] allPrice = StringUtils.strTobytes("\n\nВсього до оплати:");
                                    list.add(allPrice);
                                    byte[] allSumm = StringUtils.strTobytes(formatValue(printCheck.finalSum + " грн.", 31, false) + "\n\n");
                                    list.add(allSumm);

                                    list.add(DataForSendToPrinterPos80.selectOrCancelBoldModel(2));
                                    list.add(DataForSendToPrinterPos80.selectAlignment(1));
                                    byte[] thanks = StringUtils.strTobytes("Дякуємо за покупку!\nСмачного!\n\n");
                                    list.add(thanks);

                                    byte[] noFiscal = StringUtils.strTobytes("не фіскальний чек\n");
                                    list.add(noFiscal);

                                    list.add(DataForSendToPrinterPos80.printAndFeedLine());
                                    //cut pager
                                    list.add(DataForSendToPrinterPos80.selectCutPagerModerAndCutPager(66, 1));

                                    callback.success("Print success");
                                    repeatCheckPrint = false;

                                    return list;
                                }
                            });

                    Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        if (this.repeatCheckPrint) {
                            this.reConnect("check", args, callback);
                        } else {
                            this.repeatCheckPrint = true;
                        }
                    }, 2000);
                } catch (Exception ex) {
                    callback.error("Something went wrong " + ex);
                }
            } else {
                callback.error("Please do not pass null value");
            }
        } else {
            callback.error("Connection with printer was lost");
        }
    }

    private void printWaste(JSONArray args, CallbackContext callback) {
        if (args != null) {
            PrintWaste waste = this.parseJsonWaste(args);
            try {
                binder.WriteSendData(
                        new TaskCallback() {
                            @Override
                            public void OnSucceed() {

                            }

                            @Override
                            public void OnFailed() {
                                callback.error("Disconnect failed");
                            }
                        }, new ProcessData() {
                            @Override
                            public List<byte[]> processDataBeforeSend() {

                                List<byte[]> list = new ArrayList<byte[]>();
                                //initialize the printer
                                list.add(DataForSendToPrinterPos80.initializePrinter());

                                list.add(DataForSendToPrinterPos80.selectAlignment(0));
                                list.add(DataForSendToPrinterPos80.selectCharacterSize(0));
                                list.add(DataForSendToPrinterPos80.selectOrCancelBoldModel(1));
                                byte[] wasteName = StringUtils.strTobytes("Накладна #__________від " + waste.day + "." + waste.month + "." + waste.year + "p.\n");
                                list.add(wasteName);

                                byte[] fop_ipn = StringUtils.strTobytes("Відпущено: ФОП " + waste.fop + " РНОКПП: " + waste.ipn + "\n");
                                list.add(fop_ipn);

                                byte[] geted = StringUtils.strTobytes("Одержано: " + waste.provider + '\n');
                                list.add(geted);

                                byte[] employee = StringUtils.strTobytes("Через: " + waste.employee + '\n');
                                list.add(employee);

                                byte[] doc = StringUtils.strTobytes("Довіреність: сер.___№____від<<___>><<___>>" + waste.year + "p.\n" + '\n');
                                list.add(doc);

                                byte[] header = StringUtils.strTobytes("#          Назва       Од. К-сть   Ціна   Сума  \n");
                                list.add(header);

                                byte[] subheader = StringUtils.strTobytes("                                 без ПДВ без ПДВ\n");
                                list.add(subheader);

                                byte[] devider = new byte[48];
                                Arrays.fill(devider, (byte) 173);
                                list.add(devider);

                                int idLength = 4;
                                int nameLenght = 19;
                                int typeLength = 4;
                                int quantityLenght = 5;
                                int priceLenght = 8;
                                int sumLenght = 8;

                                for (int i = 0; i < waste.products.size(); i++) {
                                    PrintProduct pr = waste.products.get(i);
                                    byte[] id = StringUtils.strTobytes(formatValue(Integer.toString(i + 1), idLength, true));
                                    list.add(id);

                                    if (pr.name.length() > nameLenght) {

                                        String[] arr = pr.name.split(" ");
                                        String name = "";

                                        String tempWord = "";
                                        for (int j = 0; j < arr.length; j++) {
                                            if (tempWord.length() + arr[j].length() > nameLenght) {
                                                tempWord += "\n";
                                                name += tempWord;
                                                tempWord = "    " + arr[j] + " ";

                                                if (j == arr.length - 1) {
                                                    name += "    " + arr[j];
                                                }
                                            } else {
                                                if (j == arr.length - 1) {
                                                    tempWord += arr[j];
                                                    name += tempWord;
                                                } else {
                                                    tempWord += arr[j] + " ";
                                                }
                                            }
                                        }

                                        String[] nameArr = name.split("\n");
                                        String lastWord = nameArr[nameArr.length - 1];
                                        String formatedWord = formatValue(lastWord, nameLenght + 4, true);
                                        String fullName = name.replace(lastWord, formatedWord);

                                        byte[] nameByte = StringUtils.strTobytes(fullName);
                                        list.add(nameByte);
                                    } else {
                                        byte[] name = StringUtils.strTobytes(formatValue(pr.name, nameLenght, true));
                                        list.add(name);
                                    }

                                    byte[] type = StringUtils.strTobytes(formatValue(pr.type, typeLength, false));
                                    list.add(type);

                                    byte[] quantity = StringUtils.strTobytes(formatValue(pr.quantity, quantityLenght, false));
                                    list.add(quantity);

                                    byte[] price = StringUtils.strTobytes(formatValue(pr.price, priceLenght, false));
                                    list.add(price);

                                    byte[] sum = StringUtils.strTobytes(formatValue(pr.sum, sumLenght, false) + "\n");
                                    list.add(sum);
                                }
                                list.add(devider);

                                byte[] withoutPDV = StringUtils.strTobytes("Всього без ПДВ:" + formatValue(waste.sum + "грн.", 33, false) + "\n\n");
                                list.add(withoutPDV);

                                byte[] pdv = StringUtils.strTobytes("ПДВ(____%)\n");
                                list.add(pdv);

                                byte[] withPDV = StringUtils.strTobytes("Загальна сума з ПДВ:" + formatValue("________.___грн.", 28, false) + "\n\n");
                                list.add(withPDV);

                                byte[] count = StringUtils.strTobytes("Всього відпущено " + waste.positionCount + " найменувань(ня)\n");
                                list.add(count);

                                byte[] onSum = StringUtils.strTobytes("на суму " + waste.money + "грн., " + waste.cent + "коп., в т.ч ПДВ=_______грн.\n\n");
                                list.add(onSum);

                                byte[] fotter = StringUtils.strTobytes("Директор_______________  Гол.Бух._______________\n\n");
                                list.add(fotter);

                                byte[] fotter2 = StringUtils.strTobytes("Відпустив______________  Одержав________________\n");
                                list.add(fotter2);

                                list.add(DataForSendToPrinterPos80.printAndFeedLine());
                                //cut pager
                                list.add(DataForSendToPrinterPos80.selectCutPagerModerAndCutPager(66, 1));

                                callback.success("Print success");

                                repeatWastePrint = false;

                                return list;
                            }
                        });

                Handler handler = new Handler();
                handler.postDelayed(() -> {
                    if (this.repeatWastePrint) {
                        this.reConnect("waste", args, callback);
                    } else {
                        repeatWastePrint = true;
                    }
                }, 2000);
            } catch (Exception ex) {
                callback.error("Something went wrong " + ex);
            }
        } else {
            callback.error("Please donot pass null value");
        }
    }

    private void reConnect(String action, JSONArray args, CallbackContext callback) {
        if (args != null) {
            try {
                JSONObject argsObj = args.getJSONObject(0).getJSONObject(action);
                String ipAddress = argsObj.getString("IP");
                if (ipAddress.equals(null) || ipAddress.equals("")) {
                    callback.error("IP address is empty");
                } else {
                    binder.ConnectNetPort(ipAddress, 9100, new TaskCallback() {
                        @Override
                        public void OnSucceed() {
                            ISCONNECT = true;
                            switch (action) {
                                case "check":
                                    repeatCheckPrint = false;
                                    printCheck(args, callback);
                                    break;
                                case "waste":
                                    repeatWastePrint = false;
                                    printWaste(args, callback);
                                    break;
                            }
                        }

                        @Override
                        public void OnFailed() {
                            ISCONNECT = false;
                            callback.error("Connect failed");
                        }
                    });
                }
            } catch (Exception ex) {
                callback.error("Something went wrong " + ex);
            }
        } else {
            callback.error("Please donot pass null value");
        }
    }

    private PrintCheck parseJsonCheck(JSONArray args) {
        PrintCheck printCheck = new PrintCheck();
        try {
            JSONObject checkObj = args.getJSONObject(0).getJSONObject("check");
            printCheck.shopName = checkObj.getString("shopName");
            printCheck.fop = checkObj.getString("fop");
            printCheck.ipn = checkObj.getString("ipn");
            printCheck.employee = checkObj.getString("employee");
            printCheck.saleDate = checkObj.getString("saleDate");
            printCheck.client = checkObj.getString("client");
            printCheck.discount = checkObj.getString("discount");
            printCheck.bonusPay = checkObj.getString("bonusPay");
            printCheck.finalSum = checkObj.getString("finalSum");
            printCheck.products = new ArrayList<PrintProduct>();
            JSONArray arr = checkObj.getJSONArray("products");

            for (int i = 0; i < arr.length(); i++) {
                PrintProduct pr = new PrintProduct();
                pr.name = arr.getJSONObject(i).getString("name");
                pr.price = arr.getJSONObject(i).getString("price");
                pr.quantity = arr.getJSONObject(i).getString("quantity");
                pr.sum = arr.getJSONObject(i).getString("sum");
                printCheck.products.add(pr);
            }

        } catch (Exception e) {
            Log.e("JSON/Parse", e.getMessage());
        }

        return printCheck;
    }

    private PrintWaste parseJsonWaste(JSONArray args) {
        PrintWaste waste = new PrintWaste();
        try {
            JSONObject checkObj = args.getJSONObject(0).getJSONObject("waste");
            waste.fop = checkObj.getString("fop");
            waste.ipn = checkObj.getString("ipn");
            waste.employee = checkObj.getString("employee");
            waste.provider = checkObj.getString("provider");
            waste.day = checkObj.getString("day");
            waste.month = checkObj.getString("month");
            waste.year = checkObj.getString("year");
            waste.sum = checkObj.getString("sum");
            waste.money = checkObj.getString("money");
            waste.cent = checkObj.getString("cent");
            waste.positionCount = checkObj.getInt("positionCount");
            waste.products = new ArrayList<PrintProduct>();
            JSONArray arr = checkObj.getJSONArray("products");

            for (int i = 0; i < arr.length(); i++) {
                PrintProduct pr = new PrintProduct();
                pr.name = arr.getJSONObject(i).getString("name");
                pr.price = arr.getJSONObject(i).getString("price");
                pr.quantity = arr.getJSONObject(i).getString("quantity");
                pr.sum = arr.getJSONObject(i).getString("sum");
                pr.type = arr.getJSONObject(i).getString("type");
                waste.products.add(pr);
            }

        } catch (Exception e) {
            Log.e("JSON/Parse", e.getMessage());
        }

        return waste;
    }

    private String formatValue(String value, int fieldLength, boolean isName) {
        int valLength = value.length();
        int lengthDiff = fieldLength - valLength;
        for (int i = 1; i <= lengthDiff; i++) {
            if (isName) {
                value = value + " ";
            } else {
                value = " " + value;
            }
        }
        return value;
    }

    private void printBarcode(JSONArray args, CallbackContext callback) {
        binder.WriteSendData(new TaskCallback() {
            @Override
            public void OnSucceed() {
                callback.success("01234567890");
            }

            @Override
            public void OnFailed() {

            }
        }, new ProcessData() {
            @Override
            public List<byte[]> processDataBeforeSend() {
                List<byte[]> list = new ArrayList<byte[]>();
                //initialize the printer
                list.add(DataForSendToPrinterPos80.initializePrinter());
                //select alignment
                list.add(DataForSendToPrinterPos80.selectAlignment(1));
                //select HRI position
                list.add(DataForSendToPrinterPos80.selectHRICharacterPrintPosition(02));
                //set the width
                list.add(DataForSendToPrinterPos80.setBarcodeWidth(3));
                //set the height ,usually 162
                list.add(DataForSendToPrinterPos80.setBarcodeHeight(162));
                //print barcode ,attention,there are two method for barcode.
                //different barcode type,please refer to the programming manual
                //UPC-A
                list.add(DataForSendToPrinterPos80.printBarcode(69, 10, "B123456789"));

                list.add(DataForSendToPrinterPos80.printAndFeedLine());

                return list;
            }
        });
    }

    private void printQRcode(JSONArray args, CallbackContext callback) {
        binder.WriteSendData(new TaskCallback() {
            @Override
            public void OnSucceed() {

            }

            @Override
            public void OnFailed() {
                ISCONNECT = false;
            }
        }, new ProcessData() {
            @Override
            public List<byte[]> processDataBeforeSend() {
                List<byte[]> list = new ArrayList<byte[]>();
                //initialize the printer
                list.add(DataForSendToPrinterPos80.initializePrinter());
                //select alignment
                list.add(DataForSendToPrinterPos80.selectAlignment(1));

                //set the size
                list.add(DataForSendToPrinterPos80.SetsTheSizeOfTheQRCodeSymbolModule(3));
                //set the error correction level
                list.add(DataForSendToPrinterPos80.SetsTheErrorCorrectionLevelForQRCodeSymbol(48));
                //store symbol data in the QRcode symbol storage area
                list.add(DataForSendToPrinterPos80.StoresSymbolDataInTheQRCodeSymbolStorageArea(
                        "Welcome to Printer Technology to create advantages Quality to win in the future"
                ));

                //Prints The QRCode Symbol Data In The Symbol Storage Area
                list.add(DataForSendToPrinterPos80.PrintsTheQRCodeSymbolDataInTheSymbolStorageArea());
                //print
                list.add(DataForSendToPrinterPos80.printAndFeedLine());
                //or else you could use the simple encapsulation method
                //but different ，Call the step method above，the storage data din't clean up ,
                //call PrintsTheQRCodeSymbolDataInTheSymbolStorageArea，print，you don't have set the content of qrcode again
                //Equivalent to resetting the qrcode contents in the cache every time

                //list.add(DataForSendToPrinterPos80.printQRcode(3, 48, "www.xprint.net"));

                return list;
            }
        });
    }
}

class PrintCheck {
    String shopName;
    String fop;
    String ipn;
    String employee;
    String saleDate;
    String client;
    String discount;
    String bonusPay;
    String finalSum;
    ArrayList<PrintProduct> products;
}

class PrintProduct {
    String name;
    String price;
    String quantity;
    String sum;
    String type;
}

class PrintWaste {
    String fop;
    String ipn;
    String employee;
    String provider;
    String day;
    String month;
    String year;
    String sum;
    String money;
    String cent;
    Integer positionCount;
    ArrayList<PrintProduct> products;

}
